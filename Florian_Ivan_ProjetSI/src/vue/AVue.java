package vue;

import modele.mobiles.IMobile;
import processing.core.PApplet;
import processing.core.PImage;

public abstract class AVue implements IVue {
	protected IMobile mobile;
	protected PApplet fenetre;
	protected PImage image;
	
	public AVue(PApplet fenetre, IMobile mobile){
		this.fenetre=fenetre;
		this.mobile=mobile;
	}
	
	@Override
	public void dessiner() {
	fenetre.image(this.image, mobile.getPosition().getXpix(), mobile.getPosition().getYpix());
	}
	
}
