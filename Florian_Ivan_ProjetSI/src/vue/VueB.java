package vue;

import modele.mobiles.IMobile;
import modele.mobiles.Position;
import processing.core.PApplet;
import processing.core.PImage;

public class VueB extends AVue {

	public VueB(PApplet fenetre, IMobile mobile) {
		super(fenetre, mobile);
		image = fenetre.loadImage("./images/ennemis/spaceinvader2.png");
		image.resize(90,0);
	}

}
