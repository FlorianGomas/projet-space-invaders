package vue;

import modele.mobiles.IMobile;
import modele.mobiles.Position;
import processing.core.PApplet;
import processing.core.PImage;

public class VueA extends AVue {
	
	public VueA(PApplet fenetre, IMobile mobile) {
		super(fenetre, mobile);
		image = fenetre.loadImage("./images/ennemis/spaceinvader.png");
		image.resize(70,0);
	}

}
