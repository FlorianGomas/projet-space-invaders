package generateur;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import modele.mobiles.IMobile;
import modele.mobiles.MobileA;
import modele.mobiles.MobileB;
import modele.mobiles.Position;
import modele.sprites.ASprite;
import modele.sprites.Sprite;
import processing.core.PApplet;
import vue.IVue;
import vue.VueA;
import vue.VueB;

public class GenerateurVaisseaux {
	PApplet Fenetre;
	Collection<ASprite> vaisseaux;

	public GenerateurVaisseaux(PApplet fenetre, Collection<ASprite> vaisseaux) {
		super();
		Fenetre = fenetre;
		this.vaisseaux = vaisseaux;
	}

	/* Création des vaisseaux et affichage
	 * Depart ligne : Point de départ de la ligne d'affichage des sprites
	 * Fin ligne : Fin de la ligne d'affichage des sprites
	 * increment : Distance entre chaque sprite sur la ligne
	 * posY : Position de ligne sur l'axe verticale
	 * CouleurVaisseau : Type de Vue utilisée
	 * TypeMouvement : Type de Mobile utilisé	
	 */
	public void creationVaisseaux(int departLigne, int finLigne, int increment, int posY, String couleurVaisseau,
			String typeMouvement) {

		List<Position> liste = creationListe(departLigne, finLigne, increment, posY);

		// Initialisation des mobiles
		IMobile deplacement = null;
		double incrementA = 0;
		double incrementB = 0;
		for (Position pos : liste) {
			if (typeMouvement.equals("ligne")) {
				deplacement = new MobileA(pos);
			} else if (typeMouvement.equals("courbe")) {
				deplacement = new MobileB(pos, incrementA, incrementB);
				// Les increments affectent les sinusoïdes que suivent les vaisseaux
				incrementA += 0.2;
				incrementB += 0.3;
			}
			// Création des sprites
			IVue monstre = null;
			if (couleurVaisseau.equals("vert")) {
				monstre = new VueA(Fenetre, deplacement);
			} else if (couleurVaisseau.equals("rouge")) {
				monstre = new VueB(Fenetre, deplacement);

			}
			Sprite ship = new Sprite(deplacement, monstre);
			vaisseaux.add(ship);
		}
	}


	// Création de la liste de position des vaisseaux
	private List<Position> creationListe(int depart, int fin, int incr, int Y) {
		List<Position> liste = new ArrayList<>();
		while (depart <= fin) {
			liste.add(new Position(depart, Y));
			depart += incr;
		}
		return liste;
	}

}
