package modele.sprites;

import modele.mobiles.IMobile;
import modele.mobiles.Position;
import vue.IVue;

public abstract class ASprite implements IVue, IMobile {

	@Override
	public abstract void deplacer();

	@Override
	public abstract void dessiner();
	
}
