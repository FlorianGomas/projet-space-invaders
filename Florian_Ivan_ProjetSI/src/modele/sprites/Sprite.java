package modele.sprites;

import modele.mobiles.IMobile;
import modele.mobiles.Position;
import vue.IVue;

public class Sprite extends ASprite {
	IMobile mobile;
	IVue vue;

	public Sprite(IMobile mobile, IVue vue){
		this.mobile=mobile;
		this.vue=vue;
	}
	
	@Override
	public void deplacer() {
		mobile.deplacer();
	}

	@Override
	public void dessiner() {
		vue.dessiner();
	}

	@Override
	public Position getPosition() {
		return mobile.getPosition();
	}

}
