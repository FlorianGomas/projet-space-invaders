package modele.mobiles;


public class MobileA implements IMobile{
	Position position;
	float angle = 0;
	int posX = 0;
	
	public MobileA(Position position){
		this.position = position;
		posX = position.getXpix();
	}

	
	
	@Override
	public void deplacer() {
		 angle += 0.02;
		  int mouvement = (int) (200*(float) Math.sin(angle));
		  position.setXpix(posX+mouvement);
	}

	
	@Override
	public Position getPosition() {
		return position;
	}

}
