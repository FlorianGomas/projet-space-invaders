package modele.mobiles;

import controleur.FenetreControleur;

public class MobileB implements IMobile {
	Position position;
	private float angleA;
	private float angleB;
	private int posX = 0;
	private int posY = 0;
	private double incrementA = 0;
	private double incrementB = 0;

	public MobileB(Position position, double incrementA, double incrementB) {
		this.position = position;
		posX = position.getXpix();
		posY = position.getYpix();
		this.incrementA = incrementA;
		this.incrementB = incrementB;
	}

	@Override
	public void deplacer() {

		angleA += 0.05;
		int mouvementVertical = (int) (6* (float) Math.sin(angleA+incrementA));
		position.setYpix(posY += mouvementVertical);
		angleB += 0.03;
		int mouvementHorizontal = (int) (14 * (float) Math.sin(angleB+incrementB));
		position.setXpix(posX += mouvementHorizontal);

	}

	@Override
	public Position getPosition() {
		return position;
	}

}
