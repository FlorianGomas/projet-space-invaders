package controleur;
import java.util.ArrayList;
import java.util.Collection;

import modele.sprites.ASprite;
import modele.sprites.Sprite;
import processing.core.PApplet;
import processing.core.PImage;

import generateur.GenerateurVaisseaux;

public class FenetreControleur extends PApplet {
	PImage fond;
	Collection<ASprite> vaisseaux = new ArrayList<ASprite>();

	public void settings() {
		size(1024, 768);
	}

	public void setup() {
		// fenetre
		frameRate(60);
		fond = loadImage("./images/fond/universFond.jpg");
		GenerateurVaisseaux vaisseauxEnnemis = new GenerateurVaisseaux(this, vaisseaux);
		vaisseauxEnnemis.creationVaisseaux(280, 700, 150, 50,"vert","ligne");
		vaisseauxEnnemis.creationVaisseaux(190, 700, 150, 100,"rouge","ligne");
		vaisseauxEnnemis.creationVaisseaux(50, 200, 50, 175,"rouge","courbe");
		vaisseauxEnnemis.creationVaisseaux(10, 200, 50, 175,"vert","courbe");
	}

	public void draw() {
		// fond
		image(fond, 0, 0);

		// vaisseaux : deplacement et dessin
		for (ASprite v : vaisseaux) {
			v.dessiner();
			v.deplacer();
		}
	}

	public static void main(String[] args) {
		PApplet.main("controleur.FenetreControleur");
	}

}
